import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:clock/app.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Clock',
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.pink,
        scaffoldBackgroundColor: Colors.black, //backgroubd color
        accentColor: Colors.pinkAccent,
        canvasColor: Colors.black, //bottomnavbar bgcolor
      ),
      home: ClockApp(title: 'Clock'),
    );
  }
}