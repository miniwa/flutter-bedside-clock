import 'package:flutter/material.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class KeySettings extends StatefulWidget {
  KeySettings({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _KeySettingsState createState() => _KeySettingsState();
}

class _KeySettingsState extends State<KeySettings> {
  final _formKey = GlobalKey<FormState>();
  String _apiKey;
  
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: Form(
          key: _formKey,
          child: Column(children: <Widget>[
            Text('Please enter an open weather map api key!'),
            TextFormField(
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter api key';
                }
                if (value.length != 32) {
                  return 'Please enter correct key';
                }
                return null;
              },
              textAlign: TextAlign.center,
              decoration: InputDecoration(hintText: "api key goes here"),
              onSaved: (val) => _apiKey = val,
            ),
            RaisedButton(
              onPressed: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                final FormState _form = _formKey.currentState;
                if (_form.validate()) {
                  _form.save();
                  await prefs.setString('owmApiKey', _apiKey);
                  Navigator.pop(context, _apiKey);
                }
              },
              child: Text('save'),
            ),
          ],)
        )
      ),
      onWillPop: () {
        return Future.value(false);
      },
    );
  }
}