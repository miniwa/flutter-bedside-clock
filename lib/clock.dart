import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:clock/keySettings.dart';

class Clock extends StatefulWidget {
  Clock({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ClockState createState() => _ClockState();
}

class _ClockState extends State<Clock> {
  Timer _clockTimer;
  Timer _weatherTimer;

  String _hour = "75"; //there is absolutely nothing wrong with all numbers being strings
  String _minute = "86";
  String _day = "20";
  String _month = "28";
  String _year = "83";

  String _weather = "Loadin...";
  String _degrees = "??";

  @override
  void initState() {
    super.initState();
    _refreshWeather();
    _clockTimer = Timer.periodic(Duration(seconds: 1), (Timer t) => _refreshClock());
    _weatherTimer = Timer.periodic(Duration(minutes: 30), (Timer t) => _refreshWeather());
  }

  @override
  void dispose() {
    _clockTimer?.cancel();
    _weatherTimer?.cancel();
    super.dispose();
  }

  void _refreshClock() {
    var _time = new DateTime.now();
    setState(() {
     _hour = _time.hour.toString().padLeft(2, '0');
     _minute = _time.minute.toString().padLeft(2, '0');
     _day = _time.day.toString().padLeft(2, '0');
     _month = _time.month.toString().padLeft(2, '0');
     _year = _time.year.toString().substring(2);
    });
  }

  Future<String> _getOwmApiKey() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String _key = prefs.getString('owmApiKey') ?? null;
    if (_key == null) {
      String _return = await Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => KeySettings()));
      debugPrint(_return);
      if (_return == null) {
        //fuck this shit, the user is an idiot
        exit(0);
        return "lol";
      } else {
        return _return;
      }
    } else {
      return _key;
    }
  }

  void _refreshWeather() async {
    String _key = await _getOwmApiKey();
    try {
      var _response = await http.get("https://api.openweathermap.org/data/2.5/weather?q=Nordstaden,SE&appid=" + _key);
      if (_response.statusCode == 200) {
        var _jsonResponse = jsonDecode(_response.body);
        setState(() {
          _weather = _jsonResponse["weather"][0]["description"];
          _degrees = (double.parse(_jsonResponse["main"]["temp"].toString()) - 273.15).toInt().toString();
        });
      } else {
        setState(() {
         _weather = "error"; 
        });
      }
    } catch (e) {
      setState(() {
        _weather = "error"; 
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                '$_hour:$_minute',
                style: TextStyle(fontSize: 100), //TODO: figure out uhh automatic text sizing
                textAlign: TextAlign.center,
              ),
              Text(
                '$_day/$_month/$_year',
                style: TextStyle(fontSize: 42),
                textAlign: TextAlign.center,
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                '$_weather',
                style: TextStyle(fontSize: 42),
                textAlign: TextAlign.center,
              ),
              Text(
                '$_degrees\c',
                style: TextStyle(fontSize: 32),
                textAlign: TextAlign.center,
              ),
            ],
          )
        ],
      ),
    );
  }
}