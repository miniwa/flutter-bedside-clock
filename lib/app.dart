import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:clock/clock.dart';

class ClockApp extends StatefulWidget {
  ClockApp({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ClockAppState createState() => _ClockAppState();
}

class _ClockAppState extends State<ClockApp> {
  int _selectedPageIndex = 0;
  
  static List<Widget> _pageOptions = <Widget>[
    Clock(),
    Clock(),
    Clock(),
    Clock(),
  ];

  void _onPageItemTapped(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }
  
  BottomNavigationBarItem bottomNavBarItem(
      BuildContext context, IconData icon, String title) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      title: Text(
        title,
        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      body: AnimatedSwitcher(
        child: _pageOptions.elementAt(_selectedPageIndex),
        duration: const Duration(milliseconds: 200),
        transitionBuilder: (Widget child, Animation<double> animation) {
            return FadeTransition(child: child, opacity: animation);
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          bottomNavBarItem(context, Icons.timer, 'Clock'),
          bottomNavBarItem(context, Icons.cloud, 'Weather'),
          bottomNavBarItem(context, Icons.alarm, 'Alarm'),
          bottomNavBarItem(context, Icons.network_wifi, 'Network'),
        ],
        currentIndex: _selectedPageIndex,
        onTap: _onPageItemTapped,
        type: BottomNavigationBarType.fixed,
      ),
    );
  }
}
